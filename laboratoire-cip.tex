%%% Copyright (C) 2019 Vincent Goulet, David Beauchemin
%%%
%%% Ce fichier fait partie du projet
%%% «Initiation au calcul informatique de pointe»
%%% https://gitlab.com/vigou3/laboratoire-cip
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.

\documentclass[letterpaper,11pt,oneside,article,x11names,french]{memoir}
  \usepackage{natbib,url}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage[absolute]{textpos}
  \usepackage[shortlabels]{enumitem}
  \usepackage{awesomebox}

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Initiation au calcul informatique de pointe}
  \author{Vincent Goulet \and David Beauchemin}
  \date{Version {\protect\fullcaps\protect\year}.\protect\month}
  \renewcommand{\year}{2019}
  \renewcommand{\month}{03}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/laboratoire-cip}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Polices de caractères
  \usepackage{fontspec}
  \usepackage{unicode-math}
  \defaultfontfeatures
  {
    Scale = 0.92
  }
  \setmainfont{Lucida Bright OT}
  [
    Ligatures = TeX,
    Numbers = OldStyle
  ]
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\fullcaps{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Couleurs
  \usepackage{xcolor}
  \definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
  \definecolor{url}{rgb}{0.6,0,0}           % liens externes
  \definecolor{citation}{rgb}{0,0.5,0}      % citations

  %% Hyperliens
  \usepackage{hyperref}
  \hypersetup{%
    pdfauthor = \theauthor,
    pdftitle = \thetitle,
    colorlinks = true,
    linktocpage = true,
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Listes. Paramétrage avec enumitem.
  \setlist[enumerate]{leftmargin=*,align=left}
  \setlist[itemize]{leftmargin=*,align=left}

  %% Options de babel
  \frenchbsetup{CompactItemize=false,%
    ThinSpaceInFrenchNumbers=true,
    og=«, fg=»}

  %% Message de contrat de publication à insérer à la fin du document
  \AtEndDocument{\vfill\footnotesize\noindent%
    \raisebox{-2pt}{\href{http://creativecommons.org}{%
        \includegraphics[height=10pt,keepaspectratio=true]{by-sa}}} %
    {\textcopyright} Vincent Goulet, David Beauchemin. Ce document est
    publié sous licence
    \href{http://creativecommons.org/licenses/by-sa/4.0/deed.fr}{%
      Attribution-Partage dans les mêmes conditions 4.0
      International}.}

  %% Il faut charger le package lastpage après la commande précédente.
  \RequirePackage{lastpage}

  %% Numéro de page au centre en bas
  \makeoddfoot{plain}{}{\thepage\ de \pageref*{LastPage}}{}
  \pagestyle{plain}

  %% Positionnement du logo
  \TPGrid{8}{64}
  \newlength{\logoheight}
  \newlength{\logowidth}
  \setlength{\logoheight}{4\TPVertModule}
  \setlength{\logowidth}{4.727273\logoheight}

  %% Modification du titre
  \renewcommand{\maketitle}{
    \begin{textblock*}{165mm}(25mm,15mm)
      \setlength{\parindent}{0mm}
      \includegraphics[height=\logoheight,keepaspectratio]{ul_p}
    \end{textblock*}
    \vspace{18pt}
    \begin{center}
      \sffamily
      {\bfseries\Large\thetitle}\par
      \vspace{6pt}
      {\bfseries\theauthor}\par
      \vspace{6pt}
      {\mdseries\thedate}
    \end{center}}

  %% Style des titres et numérotation des sous-sections
  \chapterstyle{article}
  \renewcommand{\chaptitlefont}{\normalfont\Large\sffamily\bfseries\raggedright}
  \setsecheadstyle{\normalfont\large\sffamily\bfseries\raggedright}
  \setsubsecheadstyle{\normalfont\sffamily\bfseries\raggedright}
  \maxsecnumdepth{subsection}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \usepackage{fontawesome}
  \usepackage{relsize}
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\raisebox{-0.1ex}{\smaller\faExternalLink}}%
    \else
      \href{#1}{#2~\raisebox{-0.1ex}{\smaller\faExternalLink}}%
    \fi
  }

  %% Commandes additionnelles
  \newcommand{\code}[1]{\texttt{\upshape #1}}
  \newcommand{\pkg}[1]{\textbf{\upshape #1}}

\begin{document}

\maketitle

\chapter*{Contexte}

\link{https://www.bixi.com/fr}{BIXI~Montréal} est un organisme à but
non lucratif créé en 2014 par la Ville de Montréal pour gérer le
système de vélopartage à Montréal. Le réseau comprend \nombre{6250}
vélos et 540 stations sur le territoire montréalais, ainsi qu’à
Longueuil et Westmount.

Pour le plus grand plaisir des analystes de données, l'organisme rend
disponible ses
\link{https://www.bixi.com/fr/donnees-ouvertes}{volumineuses données}
d'achats et de déplacements. Celles-ci couvrent les années 2014 à 2018
et les huit mois d'activité du service, soit d'avril à novembre.
Seulement pour le mois d'août 2018, on dénombre plus de \nombre{950
  000} enregistrements. Le volume total des données approche le
gigaoctet.

Vous disposez des données du nombre et de la durée des locations, de
même que de la \link{https://www.bixi.com/fr/tarifs}{structure de
  tarification} du service. Il doit bien y avoir moyen de construire
un modèle de prévision des revenus pour 2019 avec tout ça!


\chapter*{Mandat}

Votre mandat pour ce laboratoire consiste à déterminer la distribution
du montant total de revenus pour chacun des mois d'activité du service
BIXI en 2019. On vous demande également obtenir la distribution
agrégée du montant total de revenus pour toute l'année.


\chapter*{Méthodologie}

Vous procéderez par simulation pour estimer la distribution du montant
total de revenus mensuel et annuel. À cette fin, vous devrez
déterminer la distribution du nombre de locations et la distribution
de la durée de celles-ci pour l'année 2019, et ce, à partir des
données des années 2014 à 2018.

Soit $T$ la variable aléatoire représentant le montant total de
revenus pour un mois donné. En simulant un échantillon aléatoire
$T_1, T_2, \dots, T_n$ de cette variable aléatoire pour une grande
valeur de $n$, vous obtiendrez un aperçu de la distribution des
revenus et vous pourrez ensuite calculer certaines quantités
d'intérêt, comme le revenu espéré.

Quant à la distribution du montant total de revenus annuel, elle
découlera simplement de l'agrégation des échantillons aléatoires de
chacun des mois d'activité du service BIXI.


\chapter*{Modélisation}

Familiers que vous êtes avec les modèles fréquence-sévérité, vous
utiliserez le modèle suivant pour la distribution de la variable
aléatoire $T$ représentant le montant total de revenus mensuel:
\begin{equation*}
  T = R_1 + \dots + R_N,
\end{equation*}
où $N$ la variable aléatoire du nombre de locations dans un mois et
$R_j$, est la variable aléatoire des revenus (ou frais, selon le point
de vue) liés à la location $j$, $j = 1, 2, \dots$. Le revenu tiré
d'une location est une fonction directe de la durée de celle-ci. Dans
la suite, nous noterons $Y_j$ la durée de la location $j$.

Sous les hypothèses\footnote{Hypothèses qui ne sont pas satisfaites
  dans le cas présent. Néanmoins, la
  \link{https://en.wikipedia.org/wiki/All_models_are_wrong}{célèbre
    maxime} de George E.~P.~Box s'applique: «Tous les modèles sont
  faux, mais certains sont utiles.»} %
que les variables aléatoires $R_1, R_2, \dots$ sont indépendantes,
identiquement distribuées et mutuellement indépendantes de la variable
aléatoire $N$, et en supposant que la distribution de $N$ est Poisson,
vous savez que la variable aléatoire $T$ est une Poisson composée.

Des âmes charitables --- auxquelles nous référerons dorénavant en tant
qu'«équipe de modélisation» --- se sont chargé de déterminer les
modèles adéquats pour la moyenne de la loi de Poisson et pour la durée
des locations.

\section*{Fréquence des locations}

Le nombre mensuel de locations a connu une forte hausse entre 2014 et
2018. Soit $\mu$ le nombre espéré de locations dans un mois. L'équipe
de modélisation a déterminé que le modèle suivant décrit de manière
acceptable l'évolution de $\mu$ dans le temps:
\begin{equation*}
  \log \mu = \alpha_0 + \alpha_1 x,
\end{equation*}
où $x$ est l'année. Ainsi,
\begin{equation*}
  N|x \sim \text{Poisson}(e^{\alpha_0 + \alpha_1 x})
\end{equation*}
et vous pouvez estimer les paramètres $\alpha_0$ et $\alpha_1$ par la
méthode du maximum de vraisemblance à partir des données
$(N_{2014}, x_{2014}), \dots, (N_{2018}, x_{2018})$.

\section*{Durée des locations}

Après analyse des données, l'équipe de modélisation est arrivée à
déterminer que la distribution du logarithme des durées de location à
l'intérieur d'un mois est approximativement normale. La moyenne de
cette distribution varie en fonction de l'année, mais la variance
demeure sensiblement la même.

À partir de ces constats, l'équipe de modélisation vous propose de
poser:
\begin{equation*}
  \log(Y_j|x) \sim N(\beta_0 + \beta_1 x, \sigma^2)
\end{equation*}
ou, de manière équivalente,
\begin{equation*}
  Y_j|x \sim \text{Log-normale}(\beta_0 + \beta_1 x, \sigma^2).
\end{equation*}
Vous estimerez ensuite $\sigma$ à partir de l'ensemble des durées de
location d'un même mois, et les paramètres $\beta_0$ et $\beta_1$ par
la méthode du maximum de vraisemblance à partir des échantillons
$(Y_{2014, 1}, x_{2014}), \dots, (Y_{2018, n}, x_{2018})$. Dans les
deux cas vous disposez pour ce faire de centaines de milliers
d'observations.

\section*{Conversion des durées en revenus}

Question de simplifier les choses, vous pouvez considérer uniquement
les tarifs pour les allers simples des accès de courte durée. De plus,
toujours par souci de simplification, la structure de tarification a
été ramenée comme suit de trois à deux paliers:
\begin{itemize}
\item tarif de base de 2,95~\$;
\item période de déplacement allouée de 30 minutes;
\item frais supplémentaires de 2,40~\$ par tranche de 15 minutes
  (entamée) après la période de déplacement allouée.
\end{itemize}

Comme les durées de location sont en secondes, la relation entre les
variables aléatoires $R_j$ et $Y_j$ est:
\begin{equation*}
  R_j = 2,95 + 2,40
  \left\lceil
    \frac{\max(Y_j - \nombre{1800}, 0)}{900}
  \right\rceil,
\end{equation*}
où $\lceil x \rceil$ représente le plus petit entier supérieur ou égal
à $x$.


\chapter*{Code informatique}

Le présent document est livré avec une mise en œuvre en R de
l'ensemble des calculs demandés ci-dessus. Vous êtes invités à étudier
attentivement le code, puis à l'exécuter avec un petit nombre de
simulations --- disons $\nombre{1000}$ --- afin de constater les défis
que pose ce projet en termes de temps de calcul et d'espace mémoire.

\tipbox{Vous avez ici une belle occasion d'utiliser l'exécution en lot
  de code R tel qu'expliqué dans \citet[section
  A.6]{Goulet:methodes-numeriques-en-actuariat:2019}.}

\chapter*{Livrables}

Vous devez produire un court rapport avec R~Markdown faisant état des
résultats de vos simulations. Le rapport doit contenir au minimum:
\begin{enumerate}
\item un titre et votre nom en entête;
\item une équation mathématique hors paragraphe;
\item la mention du nombre de simulations sur lesquelles se basent vos
  résultats (nombre pris dynamiquement à partir de l'objet R);
\item les montants espérés de revenus pour 2019 par mois et pour
  l'année entière;
\item les graphiques, tracés avec la fonction \code{hist}, de la
  distribution empirique des revenus mensuels pour les mois d'avril à
  novembre, de même que celui de la distribution des revenus totaux
  pour l'année 2019.
\end{enumerate}

\tipbox{Pour disposer plusieurs graphiques sur une grille de \meta{n}
  lignes et \meta{m} colonnes, exécutez avant la création des
  graphiques la commande \code{par(mfrow = c(\meta{n}, \meta{m}))}.}

\tipbox{Les simulations requièrent un supercalculateur, mais vous
  pouvez effectuer l'analyse des résultats et la préparation du
  rapport localement sur votre poste de travail.}

\bibliographystyle{francais}
\bibliography{laboratoire-cip.bib}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
