<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Initiation au calcul informatique de pointe

Les ordinateurs dont nous disposons au quotidien sont toujours plus puissants. Cependant, nous leur confions également la résolution de problèmes de plus en plus énormes. Résultat: il y a toujours un point où la puissance de calcul d'un ordinateur personnel devient insuffisante. La solution peut provenir du calcul haute performance. 

[*Initiation au calcul informatique de pointe*](https://gitlab.com/vigou3/laboratoire-cip/releases/) est un laboratoire (ou atelier) d'initiation à l'utilisation d'un supercalculateur pour résoudre un problème concret offert dans le cadre du cours [ACT-2002 Méthodes numériques en actuariat](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/act-2002-methodes-numeriques-en-actuariat.html) à l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

La formation est offerte en collaboration étroite avec [Calcul Québec](https://www.calculquebec.ca).

## Auteurs

Vincent Goulet, professeur titulaire, et David Beauchemin, auxiliaire d'enseignement, École d'actuariat, Université Laval 

## Licence

«Initiation au calcul informatique de pointe» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.
