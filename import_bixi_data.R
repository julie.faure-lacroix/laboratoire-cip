## Copyright (C) 2019 Vincent Goulet, David Beauchemin
##
## Ce fichier fait partie du projet
## «Initiation au calcul informatique de pointe»
## https://gitlab.com/vigou3/laboratoire-cip
##
## Cette création est mise à disposition sous licence
## Attribution-Partage dans les mêmes conditions 4.0
## International de Creative Commons.
## https://creativecommons.org/licenses/by-sa/4.0/

###
### import_bixi_data(year, month, path = "")
###
##  Importe les données ouvertes BIXI correpondant aux années et aux
##  fournis en arguments. Les vecteurs sont recyclés comme d'habitude.
##
##  Arguments
##
##  year: vecteur des années à importer.
##  month: vecteur des mois à importer.
##  path: répertoire où se trouvent les données (répertoire de travail
##    par défaut).
##
##  Valeur
##
##  Data frame de six colonnes:
##
##  start_date: date de début de la location ("Date");
##  start_station_code: code de station de départ ("factor");
##  end_date: date de fin de la location ("Date");
##  end_station_code: code de station d'arrivée ("factor");
##  duration_sec: durée de la location en secondes ("numeric");
##  is_member: utilisateur membre ou non-membre ("factor").
##
import_bixi_data <- function(year, month, path = "")
{
    files <- file.path(path,
                       paste0("OD_", year, "-",
                              formatC(month, width = 2, flag = "0"),
                              ".csv"))

    if (any(na <- !file.exists(files)))
        stop(paste("missing data file(s):", paste(files[na], collapse = ",")))

    res <- numeric(0)
    for (f in files)
        res <- rbind(res, read.data(f))
    res
}

###
### read.data(file)
###
##  Importe les fichiers de données ouvertes BIXI.
##
##  Arguments
##
##  file: nom du fichier de données à importer.
##
##  Valeur
##
##  Data frame de six colonnes:
##
##  start_date: date de début de la location ("Date");
##  start_station_code: code de station de départ ("factor");
##  end_date: date de fin de la location ("Date");
##  end_station_code: code de station d'arrivée ("factor");
##  duration_sec: durée de la location en secondes ("numeric");
##  is_member: utilisateur membre ou non-membre ("factor").
##
read.data <- function(file)
{
    if (!file.exists(file))
        stop(paste("file", file, "does not exist"))

    read.csv(file,
             colClasses = c(start_date         = "Date",
                            start_station_code = "factor",
                            end_date           = "Date",
                            end_station_code   = "factor",
                            duration_sec       = "numeric",
                            is_member          = "factor"),
             comment.char = "")
}
